from __future__ import absolute_import, print_function, unicode_literals

import sys
import os
import pickle

import PyQt5.QtWidgets as qtwidgets

from pyritze import genetics, fileio

VORNAME, GRUNDSCHULE, NATIONALITAET, GESCHLECHT, RELIGION, SCHULE, WUNSCH, AUSSCHLUSS = range(8)


def qt_getfile(defaultpath):
    fname = qtwidgets.QFileDialog.getOpenFileName(None, "Open file",
                                                  defaultpath, "csv files (*.csv)")
    return fname


def qt_message(header, message=""):
    msg = qtwidgets.QMessageBox()

    msg.setText(header)
    msg.setInformativeText(message)
    msg.setWindowTitle("pyRitze")
    msg.setStandardButtons(qtwidgets.QMessageBox.Ok)
    msg.exec_()


def _main():
    app = qtwidgets.QApplication(sys.argv)

    configfile = ".klassen_config"
    config = {
        "csvfile": "c:\\",
        "pool_groesse": 100,
        "generationen": 25,
        "kinder": 1900,
        "anzahl_klassen": 3,
    }
    if os.path.exists(configfile):
        with open(configfile, "rb") as pifi:
            config = pickle.load(pifi)

    config["csvfile"] = qt_getfile(config["csvfile"])[0]
    config["gvfile"] = config["csvfile"].replace("csv", "gv")
    with open(configfile, "wb") as pifi:
        pickle.dump(config, pifi)

    try:
        compute(config)
    except Exception as ex:
        qt_message("There was an error:", str(ex))
        raise
    qt_message("computation complete")
    sys.exit(app.exec_())


def compute(config):
    kopfzeile, schueler = fileio.read_csv(config["csvfile"], multientries=[WUNSCH, AUSSCHLUSS])
    schools = set([schueler[_p][SCHULE] for _p in schueler])
    clusters = genetics.get_clusters(schueler, WUNSCH)
    criteria = [[GESCHLECHT, ["M"]], [SCHULE, sorted(list(schools))], [RELIGION, ["Isl."]]]
    gewichte = {"Gy": 1, "Isl.": 1.5}

    out_filenames = ["{}_out{:02d}.csv".format(config["csvfile"].replace(".csv", ""), i) for i in range(10)]

    if os.path.exists(config["gvfile"]) and not os.access(config["gvfile"], os.W_OK):
        raise RuntimeError("Cannot write to '{}'. Is it still open in AcrobatReader?".format(config["gvfile"]))

    for filename in out_filenames:
        if os.path.exists(filename) and not os.access(filename, os.W_OK):
            raise RuntimeError("Cannot write to '{}'. Is it still open in Excel?".format(filename))

    print("Cluster sizes:")
    print([len(_x) for _x in clusters])
    print("Largest cluster:")
    print(sorted(list(clusters[0])))

    genetics.show_clusters(schueler, WUNSCH, config["gvfile"])

    exclusions = genetics.get_exclusions(schueler, AUSSCHLUSS)
    for _cluster in clusters:
        for pair in exclusions:
            if pair[0] in _cluster and pair[1] in _cluster:
                raise RuntimeError(
                    "Found exclusion in cluster.\n"
                    "\n"
                    "{} excluded {} but both are in cluster\n"
                    "{}".format(pair[0], pair[1], _cluster)
                )

    print(config)
    candidates = genetics.initialize_pool(
        schueler, clusters, exclusions, config["anzahl_klassen"], config["pool_groesse"])

    for i in range(config["generationen"]):
        print(str(i) + " ", end="")
        candidates = genetics.evolve(
            schueler, clusters, exclusions, config["anzahl_klassen"], criteria,
            gewichte, config["kinder"], config["pool_groesse"], candidates)

    for i in range(min(10, len(candidates))):
        fileio.write_result(
            kopfzeile, schueler, criteria, out_filenames[i], candidates[i],
            multientries=[WUNSCH, AUSSCHLUSS])


_main()
