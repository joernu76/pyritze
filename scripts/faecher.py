from __future__ import absolute_import, print_function, unicode_literals

import sys

from pyritze import genetics, fileio


VORNAME, KLASSE, FACH, RELIGION, GESCHLECHT, WUNSCH = range(6)


def _main():
    csvname = sys.argv[1]

    pool_groesse = 100
    generationen = 10
    kinder = 1000
    anzahl_klassen = 2

    kopfzeile, schueler = fileio.read_csv(csvname, multientries=[WUNSCH])
    schueler = dict([(_p, schueler[_p]) for _p in schueler if schueler[_p][FACH] != "Latein"])
    clusters = genetics.get_clusters(schueler, WUNSCH)
    criteria = [[GESCHLECHT, ["M"]], [RELIGION, ["Islam"]]]
    gewichte = {"Islam": 1.5}

    print("Cluster sizes:")
    print([len(_x) for _x in clusters])
    print("Largest cluster:")
    print(sorted(list(clusters[0])))

    # genetics.show_clusters(schueler, WISH)

    candidates = genetics.initialize_pool(schueler, clusters, anzahl_klassen, pool_groesse)

    for i in range(generationen):
        print(str(i) + " ", end="")
        candidates = genetics.evolve(
            schueler, clusters, anzahl_klassen, criteria, gewichte, kinder, pool_groesse, candidates)

    for i in range(min(10, len(candidates))):
        filename = "{}_out{:02d}.csv".format(csvname.replace(".csv", ""), i)
        fileio.write_result(kopfzeile, schueler, criteria, filename, candidates[i], multientries=[WUNSCH])


_main()
