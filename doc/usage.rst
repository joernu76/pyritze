PyRitze
=======

A python package to distribute pupils to classes using simple genetic algorithms.

Data structures:
----------------

* pupils: dictionary of lists, key is the name of the pupil.
* clusters: lists of pupil names, which shall be assigned to one class.
* classes: lists of pupil names, one list for each class.

Usage:
------

* Prepare execl sheet with pupils names etc. according to examples in samples directory
* start the klassen.py or the faecher.py script.
* evaluate the output figures and class distribution proposals
* change wishes to break-up too large clusters or change weigths and criteria