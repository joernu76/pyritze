#!/bin/env python
import os
import subprocess
from setuptools import setup

DISTNAME = 'pyritze'
DESCRIPTION = 'distribute pupils on classes based on wishes'
MAINTAINER = 'Joern Ungermann'
MAINTAINER_EMAIL = 'joern.ungermann@web.de'
VERSION = '0.9.0'


def git_revision():
    try:
        git_rev = subprocess.check_output(['git', 'log', '-1', 'HEAD']).split("\n")[0].split()[1]
    except:
        git_rev = "???"
    return git_rev


def write_version_py(filename='version.py'):
    version_string = \
        '# THIS FILE IS GENERATED FROM THE PYRITZE SETUP.PY\n' \
        'VERSION = "{}"\n' \
        'GIT_REVISION = "{}"\n'.format(VERSION, git_revision())
    with open(os.path.join(os.path.dirname(__file__), "pyritze",
                           filename), 'w') as vfile:
        vfile.write(version_string)


def _main():
    write_version_py()

    setup(
        name=DISTNAME,
        description=DESCRIPTION,
        maintainer=MAINTAINER,
        maintainer_email=MAINTAINER_EMAIL,
        version=VERSION,
        test_suite="nose.collector",
        setup_requires=["flake8", "pytest"],
        install_requires=["graphviz", "numpy"],
        classifiers=[
            'Development Status :: 3 - alpha',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'Intended Audience :: Science/Research',
            'License :: OSI Approved :: BSD License',
            'Programming Language :: Python',
            'Topic :: Scientific/Engineering',
            'Operating System :: Microsoft :: Windows',
            'Operating System :: POSIX',
            'Operating System :: Unix',
            'Operating System :: MacOS',
        ],
        include_package_data=True,
        zip_safe=False,
    )

if __name__ == "__main__":
    _main()
