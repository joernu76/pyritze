from __future__ import absolute_import, print_function, unicode_literals

import graphviz
import numpy as np

THRESHOLD = 15


class BagwayError(Exception):
    """
    Error to be raised if a given operation cannot be successfully completed.
    """
    pass


def show_clusters(pupils, wish_idx, filename):
    """
    Visualize clusters using graphviz.

    :param pupils: dictionary of pupils
    :param wish_idx: index of wish names in list entries
    :param filename: name to save figure to
    """
    dot = graphviz.Digraph(comment='Clusters')
    for pupil in pupils:
        dot.node(pupil, pupil)
        for wish in [_x for _x in pupils[pupil][wish_idx] if _x in pupils]:
            dot.edge(pupil, wish)
    dot.render(filename, view=True)


def get_exclusions(pupils, exclusion_idx):
    exclusions = []
    for pupil in pupils:
        for exclusion in pupils[pupil][exclusion_idx]:
            if exclusion not in pupils:
                raise RuntimeError(
                    "There are some pupils in exclusions that are not defined as pupils.\n"
                    "\n"
                    "exclusions: {}\n".format(exclusions) + "\n"
                    "pupils: {}".format(pupils))
            exclusions.append((pupil, exclusion))
    return exclusions


def get_clusters(pupils, wish_idx):
    """
    Identify clusters of pupils that wish one another.

    :param pupils:
    :param wish_idx:
    :return:
    """
    def add_wishes(cluster, pupil):
        if pupil not in cluster:
            cluster.add(pupil)
            unknown = [_x for _x in pupils[pupil][wish_idx] if _x not in pupils]
            if len(unknown) > 0:
                print("ignored pupil", pupil, unknown)
            for wish in [_x for _x in pupils[pupil][wish_idx] if _x in pupils]:
                cluster = add_wishes(cluster, wish)
        return cluster

    clusters = []
    for pupil in pupils:
        if any([pupil in _c for _c in clusters]):
            continue
        cluster = add_wishes(set(), pupil)
        clusters.append(cluster)

    while True:
        merge = None
        for idx0 in range(len(clusters)):
            for idx1 in range(idx0 + 1, len(clusters)):
                if any([_x in clusters[idx0] for _x in clusters[idx1]]):
                    merge = idx0, idx1
                    break
            if merge is not None:
                break
        if merge is not None:
            newclusters = [_c for _i, _c in enumerate(clusters) if _i != merge[1]]
            newclusters[merge[0]].update(clusters[merge[1]])
            clusters = newclusters
        else:
            break
    return [_c[1] for _c in sorted((-len(_c), _c) for _c in clusters)]


def generate_random_classes(pupils, clusters, exclusions, max_classes, threshold=THRESHOLD):
    target = len(pupils) / float(max_classes)
    classes = [[] for _ in range(max_classes)]
    for _cluster in clusters:
        _idx = 0
        while True:
            class_idx = np.random.random_integers(0, max_classes - 1)
            if (len(classes[class_idx]) + len(_cluster) - target) < threshold:
                newclass = classes[class_idx] + list(_cluster)
                if not any(pair[0] in newclass and pair[1] in newclass for pair in exclusions):
                    classes[class_idx].extend(_cluster)
                    break
            _idx += 1
            if _idx > 100:
                raise BagwayError
    if any([len(_c) == 0 for _c in classes]):
        raise BagwayError
    classes = sorted([sorted(_class) for _class in classes])
    for _class in classes:
        if any(pair[0] in _class and pair[1] in _class for pair in exclusions):
            assert False, (_class, exclusions)
    return classes


def count(pupils, group, field, value):
    selected = [_p for _p in group if pupils[_p][field] == value]
    if len(group) == 0:
        return 1000
    return (100. * len(selected)) / len(group)


def recombine_classes(clusters, exclusions, classes, sizediff=3):
    """
    Randomly exchange two clusters of similar size (-+3) from two different classes.

    :param clusters: List of pupil clusters
    :param classes:  Pupil-class-assignment.
    :return: New pupil-class-assignment.
    """
    tries = 0
    while True:
        while True:
            tries += 1
            if tries > 30:
                raise BagwayError
            cluster1 = np.random.random_integers(0, len(clusters) - 1)
            size = len(clusters[cluster1])
            class1 = get_classes_idx(clusters, classes, cluster1)
            clusters_p = [(_idx, _cluster) for _idx, _cluster in enumerate(clusters)
                          if (abs(len(_cluster) - size) <= sizediff and
                              _idx != cluster1 and
                              get_classes_idx(clusters, classes, _idx) != class1)]
            if len(clusters_p) == 0:
                continue
            idx = np.random.random_integers(0, len(clusters_p) - 1)
            cluster2 = clusters_p[idx][0]
            break
        class2 = get_classes_idx(clusters, classes, cluster2)
        new_classes = [_x for _x in classes]
        new_classes[class1] = [_p for _p in classes[class1] if _p not in clusters[cluster1]] + list(clusters[cluster2])
        new_classes[class2] = [_p for _p in classes[class2] if _p not in clusters[cluster2]] + list(clusters[cluster1])
        new_classes = sorted([sorted(_class) for _class in new_classes])

        if any(pair[0] in _class and pair[1] in _class for pair in exclusions for _class in new_classes):
            continue
        break
    for _class in new_classes:
        if any(pair[0] in _class and pair[1] in _class for pair in exclusions):
            assert False, (_class, exclusions)
    return new_classes


def get_classes_idx(clusters, classes, cluster):
    pupil = list(clusters[cluster])[0]
    for idx, cla in enumerate(classes):
        if pupil in cla:
            return idx
    assert False


def move_cluster(clusters, exclusions, classes):
    """
    Move one random cluster from one class to a different class. No class may be completely empty.

    :param clusters: Set of pupil clusters
    :param classes:  Lists of pupils for each class
    :return: New pupil to class assignment.
    """
    tries = 0
    while True:
        tries += 1
        if tries > 30:
            raise RuntimeError
        cluster1 = np.random.random_integers(0, len(clusters) - 1)
        class1 = get_classes_idx(clusters, classes, cluster1)
        class2 = [_c for _c in range(len(classes)) if _c != class1][np.random.random_integers(0, len(classes) - 2)]
        new_classes = [_x for _x in classes]
        new_classes[class1] = [_p for _p in classes[class1] if _p not in clusters[cluster1]]
        new_classes[class2] = classes[class2] + list(clusters[cluster1])
        new_classes = sorted([sorted(_class) for _class in new_classes])
        if len(new_classes[class1]) == 0:
            continue
        if any(pair[0] in _class and pair[1] in _class for pair in exclusions for _class in new_classes):
            continue
        break
    for _class in new_classes:
        if any(pair[0] in _class and pair[1] in _class for pair in exclusions):
            assert False, (_class, exclusions)
    return new_classes


def evaluate_classes(pupils, classes, criteria):
    result = {}
    for key, values in criteria:
        for value in values:
            average = count(pupils, pupils.keys(), key, value)
            result[value] = [round(count(pupils, _class, key, value) - average, 2) for _class in classes]
    return result


def compute_cost(pupils, classes, criteria, weights=None):
    if weights is None:
        weights = {}
    cri = evaluate_classes(pupils, classes, criteria)
    cost = sum([sum(weights.get(_x, 1) * np.asarray(cri[_x]) ** 2) for _x in cri])
    avg = len(pupils) / float(len(classes))
    cost += weights.get("class_size", 1000) * sum([(len(_c) - avg) ** 2 for _c in classes])
    return cost


def initialize_pool(pupils, clusters, exclusions, max_classes, poolsize):
    """
    Generate a random set of pupil to class assignments
    :param pupils: List of pupil names
    :param clusters: List of pupil clusters that shall be within one class
    :param max_classes: Number of classes to generate
    :param poolsize: Number of class assignments to generate
    :return: Random class assignments
    """
    candidates = []
    while len(candidates) < poolsize:
        try:
            candidates.append(generate_random_classes(pupils, clusters, exclusions, max_classes))
        except BagwayError:
            pass
    return candidates


def evolve(pupils, clusters, exclusions, max_classes, criteria, weights, children, poolsize, candidates):
    """
    Apply genetic recombination to classes to identify new optimal combinations.

    :param pupils: List of pupil names
    :param clusters:  List of pupil clusters that shall be within one class
    :param max_classes: Number of classes to generate
    :param criteria:
    :param weights:
    :param children: Number of new children to generate randomly.
    :param poolsize: Number of culled "children" assignments to return in the end
    :param candidates: Current set of "parent" assignments.
    :return: New list of candidates
    """
    nr_expand = 5
    len_candidates = len(candidates)
    new_candidates = [_x for _x in candidates]
    for _ in range(nr_expand):
        candidates = [_x for _x in new_candidates]
        for _ in range(children // (2 * nr_expand + 1)):
            idx = np.random.random_integers(0, len(candidates) - 1)
            try:
                new_candidates.append(move_cluster(clusters, exclusions, candidates[idx]))
            except BagwayError:
                pass
            idx = np.random.random_integers(0, len(candidates) - 1)
            try:
                new_candidates.append(recombine_classes(clusters, exclusions, candidates[idx]))
            except BagwayError:
                pass
    new_candidates.extend(initialize_pool(
        pupils, clusters, exclusions, max_classes, poolsize + children - len(new_candidates)))
    generated = len(new_candidates)
    new_candidates = [eval(_x) for _x in list(set([repr(_x) for _x in new_candidates]))]
    new_costs = sorted([(compute_cost(pupils, _c, criteria, weights), _c) for _c in new_candidates])

    print(", ".join("{:.1f}".format(_x[0]) for _x in new_costs[:5]), len_candidates, len(new_costs), generated)
    new_costs = [_c for _, _c in new_costs]

    quarter = poolsize // 4
    half = len(new_costs) // 2
    return new_costs[:quarter] + \
        [new_costs[i] for i in sorted(set(np.random.random_integers(quarter, half, 2 * quarter)))] + \
        [new_costs[i] for i in sorted(set(np.random.random_integers(half, len(new_costs) - 1, quarter)))]
