from __future__ import absolute_import, print_function, unicode_literals

import codecs
import os
import pprint

import numpy as np

from pyritze import genetics


def read_csv(filename, multientries=None):
    """
    Reads in pupil data from a CSV Excel Table sheet.

    """
    if multientries is None:
        multientries = []
    result = {}

    if not os.path.exists(filename):
        raise RuntimeError("Input file '{}' not found!".format(filename))
    with codecs.open(filename, 'r', encoding="Windows-1252") as in_file:
        lines = in_file.readlines()
    header = [_x.strip().capitalize() for _x in lines[0].lower().strip().split(";")]
    for line in lines[1:]:
        rows = line.lower().strip().split(";")
        name = rows[0].capitalize()
        entry = [x.capitalize() for x in rows[1:]]
        for idx in multientries:
            entry[idx] = [_x.strip().capitalize() for _x in entry[idx].lower().split(",")]
            if entry[idx] == ['']:
                entry[idx] = []
        result[name] = entry
    return header, result


def anonymize(filename, header, result, multientries=None):
    if multientries is None:
        multientries = []
    result2 = {}
    name_map = {}
    for x in result:
        while True:
            name = ""
            for i in range(5):
                name += chr(ord("a") + np.random.random_integers(0, 25))
            name = name.capitalize()
            if name not in name_map:
                break
        name_map[x] = name
    name_map[""] = ""
    for x in result:
        entry = result[x]
        name = ""
        for i in range(5):
            name += chr(ord("a") + np.random.random_integers(0, 25))
        name = name.capitalize()
        entry[0] = name
        entry[6] = [name_map[_x] for _x in entry[6] if _x in name_map]
        result2[name_map[x]] = entry
    with codecs.open(filename + "2", 'w', encoding="Windows-1252") as outfile:
        outfile.write(";".join(header) + "\n")
        for pup in result2:
            entry = [_x for _x in result2[pup]]
            for idx in multientries:
                entry[idx] = ", ".join(entry[idx])
            outfile.write(";".join([pup] + entry) + "\n")


def write_result(header, pupils, criteria, filename, solution, multientries=None):
    if multientries is None:
        multientries = []
    pprint.pprint([len(_c) for _c in solution])
    pprint.pprint(genetics.evaluate_classes(pupils, solution, criteria))
    with codecs.open(filename, "w", encoding="Windows-1252") as outfile:
        for cla in solution:
            outfile.write(";".join(header) + "\n")
            for pup in cla:
                entry = [_x for _x in pupils[pup]]
                for idx in multientries:
                    entry[idx] = ", ".join(entry[idx])
                outfile.write(";".join([pup] + entry) + "\n")

            avg = ["" for _ in range(max([2 + _x for _x, _ in criteria]))]
            avg[0] = str(len(cla))
            for idx, value in criteria:
                avg[1 + idx] = "{}:{:04.1f}%".format(value[0], genetics.count(pupils, cla, idx, value[0]))
            outfile.write(";".join(avg) + "\n")
            outfile.write("\n")
