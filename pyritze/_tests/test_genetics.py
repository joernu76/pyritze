from __future__ import absolute_import, print_function, unicode_literals
import os
import pyritze

VORNAME, GRUNDSCHULE, NATIONALITAET, GESCHLECHT, RELIGION, SCHULE, WUNSCH, AUSSCHLUSS = range(8)


def test_klassen():
    pool_groesse = 100
    generationen = 25
    kinder = 2900
    anzahl_klassen = 3
    csvname = os.path.join(os.path.dirname(__file__), "..", "..", "doc", "samples", "klassen.csv")
    kopfzeile, schueler = pyritze.fileio.read_csv(csvname, multientries=[WUNSCH])
    ausschluss = pyritze.genetics.get_exclusions(schueler, AUSSCHLUSS)

    schools = set([schueler[_p][SCHULE] for _p in schueler])
    clusters = pyritze.genetics.get_clusters(schueler, WUNSCH)
    criteria = [[GESCHLECHT, [u"M"]], [SCHULE, sorted(list(schools))], [RELIGION, [u"Islam"]]]
    gewichte = {"Gy": 1, "Islam": 1.5}

    candidates = pyritze.genetics.initialize_pool(schueler, clusters, ausschluss, anzahl_klassen, pool_groesse)

    for i in range(generationen):
        candidates = pyritze.genetics.evolve(
            schueler, clusters, ausschluss, anzahl_klassen, criteria, gewichte, kinder, pool_groesse, candidates)

    costs = sorted([(pyritze.genetics.compute_cost(schueler, _c, criteria, gewichte), _c) for _c in candidates])
    assert costs[0][0] < 805
